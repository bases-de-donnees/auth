create schema if not exists auth
  authorization iutsd;

create table auth.user (
  id uuid default gen_random_uuid() not null,
  est_actif bit,
  prenom text,
  nom text,
  email text,
  mobile text
);

alter table auth.user
  add constraint user_pkey primary key (id);

alter table auth.user
  add column created_at timestamp with time zone default now(),
  add column updated_at timestamp with time zone;

copy auth.user (id, est_actif, prenom, nom, email, mobile)
  from '/tmp/user.csv'
  delimiter ',' csv header quote '"' encoding 'utf8';

alter table if exists auth.user owner to iutsd;

-- Login

create table auth.login (
  id uuid not null,
  motdepasse text,
  recovery_token text,
  recovery_expire timestamp with time zone
);

alter table auth.login
  add constraint login_pkey primary key (id);

alter table auth.login
  add column created_at timestamp with time zone default now(),
  add column updated_at timestamp with time zone;
