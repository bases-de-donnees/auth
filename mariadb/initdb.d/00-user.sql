create database `auth`;

-- Utilisateurs

create table `auth`.user (
  `id` uuid not null default uuid(),
  est_actif bit,
  prenom varchar(50),
  nom varchar(50),
  email varchar(100),
  mobile varchar(20),
  `created_at` timestamp not null default current_timestamp(),
  `updated_at` timestamp null default null on update current_timestamp(),
  primary key (`id`)
);

load data infile '/tmp/user.csv'
replace into table `auth`.user
fields terminated by ',' enclosed by '\"'
lines terminated by '\n'
ignore 1 lines
(id, @est_actif, prenom, nom, email, mobile);

-- Login

create table `auth`.login (
  `id` uuid not null,
  `motdepasse` varchar(255),
  `created_at` timestamp not null default current_timestamp(),
  `updated_at` timestamp null default null on update current_timestamp()
);
